//Fengchi Gao 2233597
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Bicycle1", 20, 20);
        bicycles[1] = new Bicycle("Bicycle2", 21, 25);
        bicycles[2] = new Bicycle("Bicycle3", 22, 26);
        bicycles[3] = new Bicycle("Bicycle4", 23, 30);

        for (Bicycle bicycle : bicycles) {
            System.out.println(bicycle);
        }
    }
}
