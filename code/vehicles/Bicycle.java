// Fengchi Gao 2233597
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Bicycle Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", Max Speed: " + maxSpeed;
    }


}